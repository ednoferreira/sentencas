# Sentenças #

Gerencia textos em partes no site que o administrador pode alterar.

## Instalação ## 

- $ composer require onicmspack/sentencas --prefer-source
- registre o provider em config/app.php:
- 'providers' => Onicmspack\Sentencas\SentencasServiceProvider::class,
- registre também o alias neste mesmo arquivo:
- 'aliases' => 'Sentenca' => Onicmspack\Sentencas\Models\Sentenca::class,
- composer dump
- php artisan vendor:publish
- php artisan migrate

## utilização ##

- Sentenca::get('slug', 'valor padrão caso não exista o slug');

 

