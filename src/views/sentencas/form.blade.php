@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="header">
                <h4>{{ $titulo }}</h4>
            </div>
            <div class="content">
                    @if(!isset($registro->id))
                        {!! Form::open(['url' => $caminho, 'files'=>true ]) !!}
                    @else
                        {!! Form::model($registro, ['url' => $caminho.$registro->id, 'method'=>'put', 'files'=>true]) !!}
                    @endif
                    {!! Form::hidden('id', null) !!}
                    <div class="form-group" >
                        {!! Form::label('slug', 'Slug:') !!}
                        {!! Form::text('slug', null, ['class' => 'form-control', 'autofocus required'] ) !!}
                    </div>
                    <div class="form-group" >
                        {!! Form::label('descricao', 'Descrição:') !!}
                        {!! Form::text('descricao', null, ['class' => 'form-control'] ) !!}
                    </div>
                    <div class="form-group" >
                        {!! Form::label('valor', 'Valor:') !!}
                        {!! Form::text('valor', null, ['class' => 'form-control'] ) !!}
                    </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                @include('admin._partes._botao_voltar')
                {!! Form::submit('Gravar', ['class' => 'btn btn-fill btn-wd btn-success pull-right']) !!}
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection
