<?php 
namespace Onicmspack\Sentencas;

use Onicmspack\Sentencas\Models\Sentenca;
use Onicmspack\Arquivos\Models\Arquivo as Arquivo;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Routing\Controller as Controller;
 
class SentencasController extends Controller
{
 	public $caminho = 'admin/sentencas/';
    public $views   = 'admin/vendor/sentencas/';
    public $titulo  = 'Sentenças';

    public function index()
    {
        $registros = Sentenca::all();
        return view($this->views.'.index',['registros'=>$registros],[
                    'titulo' => $this->titulo,
                    'caminho' => $this->caminho,
               ]);
    }

    public function create()
    {
        return view($this->views.'.form',[
                    'titulo' => $this->titulo,
                    'caminho' => $this->caminho,
               ]);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        // valida o cadastro:
        $validou = $this->validar_cadastro($input);
        if($validou !== true){
            return redirect($this->caminho.'create')->withInput()->withErrors($validou);
        }else{
            Sentenca::create($input);
            $request->session()->flash('alert-success', config('mensagens.registro_inserido'));
        }
        return redirect($this->caminho.'create');
    }

    public function show($id)
    {
        $registro = Sentenca::find($id);
        return view($this->views.'.form', compact('registro'),[
                    'titulo' => $this->titulo,
                    'caminho' => $this->caminho,
               ]);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        // valida a edição:
        $validou = $this->validar_cadastro($input);
        if($validou !== true){
            return $validou;
        }
        $update = Sentenca::find($id)->update($input);

        $request->session()->flash('alert-success', config('mensagens.registro_alterado'));
        return redirect($this->caminho.$id.'');
    }

    public function destroy($id)
    {
        Sentenca::find($id)->delete();
        return redirect($this->caminho);
    }

    /**
     * Verifica se o cadástro enviado é válido:
     * retorna as mensagens de erro caso dê algum ou true(passou)
     */
    public function validar_cadastro($input)
    {
         $validator = Validator::make($input, [
            'slug'   => 'required',
            'valor'  => 'required',
         ]);

         if ($validator->fails()) {
            return $validator->messages();
         }

         return true;
    }
 
}



