<?php

namespace Onicmspack\Sentencas\Models;

use Illuminate\Database\Eloquent\Model;

class Sentenca extends Model
{
    //
    protected $fillable = ['slug',
    					   'valor',
    					   'descricao'];

    public static function get($slug = '', $valor_padrao = 'Valor padrão não enviado')
    {
    	if(!empty($slug)){
    		$sentenca = Sentenca::where('slug', '=', $slug)->first();
	    	if(!isset($sentenca->id)){
                // se não existe, cria a sentença agora:
                $dados['slug'] = $slug;
                $dados['valor'] = $valor_padrao;
                $sentenca = Sentenca::create($dados);
            }
    	}

    	return $sentenca->valor;
    }
}
