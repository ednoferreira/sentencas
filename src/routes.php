<?php

Route::group(['middleware' => ['web','auth', 'authorization'], 'prefix' => 'admin' ], function () {
	// Menu:
	Route::resource('sentencas', 'Onicmspack\Sentencas\SentencasController');
});