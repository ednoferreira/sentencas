<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSentencasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sentencas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('valor')->nullable();
            $table->string('descricao')->nullable();
            $table->timestamps();

            // adiciona o indice slug para a busca ser mais rápida:
            $table->index('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sentencas');
    }
}
